<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Websites e Sistemas">
        <meta name="author" content="Marcos Fabre Jr - Soluções em TI">
        <link rel="shortcut icon" href="assets/ico/favicon.png">

        <title>Alvo Desenvolvimento Web</title>

        <!-- Bootstrap core CSS -->

        <link href="assets/css/bootstrap.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="assets/css/main.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/icomoon.css">
        <link href="assets/css/animate-custom.css" rel="stylesheet">

        <link rel="icon" href="/assets/img/favicon.ico">

        <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>

        <script src="assets/js/jquery.min.js"></script>
        <script type="text/javascript" src="assets/js/modernizr.custom.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.js"></script>
          <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <script src="js/jquery.js"></script>
        <script src="js/creative.js"></script>
        <link rel="stylesheet" href="plugins/slick/slick.css" type="text/css">
        <script src="plugins/slick/slick.js"></script>
    </head>

    <body class="theme-font" data-spy="scroll" data-offset="0" data-target="#navbar-main">

        <div id="fb-root"></div>
        <script>(function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <div id="navbar-main">
            <!-- Fixed navbar -->
            <?php require_once 'layout/topo.php'; ?>
        </div>


        <div id="painel" class="clearfix">
            <ul class="painel-case">
                <li>
                    <a>
                        <div class="imagem" style="background-image:url(img/banner/web-1.jpg)"></div>
                        <div class="mensagem-content">

                        </div>
                    </a>
                </li>
                <li>
                    <a>
                        <div class="imagem" style="background-image:url(img/banner/web-2.jpg)"></div>
                        <div class="mensagem-content">

                        </div>
                    </a>
                </li>
                <li>
                    <a>
                        <div class="imagem" style="background-image:url(img/banner/web-3.jpg)"></div>
                        <div class="mensagem-content">

                        </div>
                    </a>
                </li>

            </ul>
        </div>

        <!-- ==== GREYWRAP ==== -->
        <div id="greywrap">
            <div class="row">
                <div class="col-lg-4 callout">
                    <img src="assets/img/bootstrap.png" height="50">
                    <h2>Bootstrap </h2>
                    <p>WebSites e Sistemas Web desenvolvidos completamente em Bootstrap,
                        otimizando seu sistema para ser visualizado em desktops, notebooks, tablets e smartphones.</p>
                </div><!-- col-lg-4 -->

                <div class="col-lg-4 callout">
                    <img src="assets/img/php.png" height="50">
                    <h2>PHP 5</h2>
                    <p> Seu sistema é desenvolvido na linguagem php, que possibilita a instalação em somente um computador que é o servidor,
                        os outros computadores acessam o servidor para utilizarem o sistema, mais praticidade para sua empresa. </p>
                </div><!-- col-lg-4 -->	

                <div class="col-lg-4 callout">
                    <img src="assets/img/mysql.png" height="50">
                    <h2>Banco de Dados Mysql</h2>
                    <p>O MySql é o banco de dados de código aberto mas popular do mundo, aqui você encontra alto desempenho, 
                        confiabilidade e desenvolvedores especialistas.</p>
                </div><!-- col-lg-4 -->	
            </div><!-- row -->
        </div><!-- greywrap -->

        <!-- ==== ABOUT ==== -->
        <div class="container" id="about" >
            <div class="row white">
                <br>
                <h1 class="centered">Serviços</h1>
                <hr>

                <div class="col-lg-6">
                    <p>Uma empresa comprometida com a satisfação de seus clientes, desenvolvemos soluções
                        inteligentes para seus negócios. Não trabalhe mais com pilhas de papel ou enormes planilhas,
                        nossos sistemas são a melhor forma de administração.<br />
                        Não importa qual é o seu ramo e serviço, analisamos suas necessidades e então solucionamos 
                        seus problemas de gerenciamento.
                    </p>
                </div><!-- col-lg-6 -->

                <div class="col-lg-6">
                    <p>Para você que deseja tornar sua empresa conhecida na web, desenvolvemos websites com designs inovadores 
                        e funcionalidades de acordo com as suas necessidades.<br />
                        Pensamos sempre no design para smartphones, que é a nova tendência do mercado, por isso, todos os websites são desevolvidos 
                        em uma linguagem que se auto ajusta de acordo com a tela em que está sendo visualizada. Seu website é bem visualizado em
                        computadores, tablets e celulares.</p>
                </div><!-- col-lg-6 -->
            </div><!-- row -->
        </div><!-- container -->

        <!-- ==== SECTION DIVIDER1 -->


        <!-- ==== SERVICES ==== -->



        <!-- ==== SECTION DIVIDER2 -->

        <!-- ==== TEAM MEMBERS ==== -->


        <!-- ==== GREYWRAP ==== -->
        <div id="greywrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 centered">
                        <img class="img-responsive" src="assets/img/macbook.png" align="">
                    </div>
                    <div class="col-lg-4">
                        <h2>Acesse seu sistema de qualquer lugar!</h2>
                        <p>Adquirindo um de nossos sistemas, você ganha a opção de funcionar na internet, assim, de onde estiver
                            pode administrar sua empresa.
                        <p>

                    </div>					
                </div><!-- row -->

            </div>
            <br>
            <br>
        </div>  

        <div class="col-lg-12">&nbsp;<br /><br /></div>

        <div id="colorBgWrap" class="img-responsive" >
            <div class="container">

                <div class="row">

                    <div class="col-lg-4 centered">
                        <br />
                        <br />
                        <br />
                        <h2>Sistemas desenvolvidos para smartphones.</h2>
                        <p>Soluções para garantir sua comodidade e facilitar a administração do
                            seu negócio.
                        <p>

                    </div>	<div class="col-lg-8 centered">
                        <img class="img-responsive" src="assets/img/iphone.png" align="">
                    </div>				
                </div><!-- row -->
            </div>
            <br>

        </div>

        <!-- ==== SECTION DIVIDER3 -->

        <!-- ==== PORTFOLIO ==== -->
        <div class="container" id="portfolio" >
            <br />
            <br />
            <div class="row">

                <h1 class="centered">CONHEÇA ALGUNS DE NOSSOS PROJETOS</h1>
                <hr>
                <br>
                <br>
            </div><!-- /row -->
            <div class="container">
                <div class="row">	

                    <!-- PORTFOLIO IMAGE 1 -->
                    <div class="col-md-4 ">
                        <div class="grid mask">
                            <figure>
                                <a data-toggle="modal" href="#myModal">	<img class="img-responsive" src="assets/img/portfolio/sgc.png" alt=""></a>
                                <figcaption>
                                    <h5>SGC</h5>
                                    <a data-toggle="modal" href="#myModal" class="btn btn-primary btn-lg">Mais</a>
                                </figcaption><!-- /figcaption -->
                            </figure><!-- /figure -->
                        </div><!-- /grid-mask -->
                    </div><!-- /col -->


                    <!-- MODAL SHOW THE PORTFOLIO IMAGE. In this demo, all links point to this modal. You should create
                         a modal for each of your projects. -->

                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Sistema Gerenciador de Computadores</h4>
                                </div>
                                <div class="modal-body">
                                    <p><img class="img-responsive" src="assets/img/portfolio/sgc.png" alt=""></p>
                                    <p>O SGC é um sistema web desenvolvido para atender à necessidade de avaliar os computadores de uma empresa em Cachoeiro de Itapemirim, o que antes era realizado através de formulários impressos gerando pilhas de papel, agora é feito por um formulário web que gera relatórios com muita objetividade e praticidade.</p>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->



                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Sistema Gerenciador de Horários para Apoio Escolar</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="col-lg-9"><img class="img-responsive" src="assets/img/portfolio/sigae.png" alt=""></div>
                                    <div class="col-lg-3"> <img class="img-responsive" src="assets/img/portfolio/sigae2.png" alt=""><br /></div>
                                    <div class="row">
                                        <p>Desenvolvido para facilitar o trabalho de empresários do ramo educacional, o sigae 
                                            proporciona módulos de gerenciamento de alunos e professores, sua função principal é criar 
                                            um horário semanal de forma rápida e prática. Disponível para smartphones.</p>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->


                    <!-- PORTFOLIO IMAGE 2 -->
                    <div class="col-md-4">
                        <div class="grid mask">
                            <figure>
                                <a data-toggle="modal" href="#myModal2">  <img class="img-responsive" src="assets/img/portfolio/sigae.png" alt=""></a>
                                <figcaption>
                                    <h5>SIGAE</h5>
                                    <a data-toggle="modal" href="#myModal2" class="btn btn-primary btn-lg">Mais</a><							</figcaption><!-- /figcaption -->
                            </figure><!-- /figure -->
                        </div><!-- /grid-mask -->
                    </div><!-- /col -->


                    <div class="col-md-4 ">
                        <div class="grid mask">
                            <figure>
                                <a data-toggle="modal" href="#myModal3">	<img class="img-responsive" src="assets/img/portfolio/sciflor.png" alt=""></a>
                                <figcaption>
                                    <h5>SciFlor</h5>
                                    <a data-toggle="modal" href="#myModal3" class="btn btn-primary btn-lg">Mais</a>
                                </figcaption><!-- /figcaption -->
                            </figure><!-- /figure -->
                        </div><!-- /grid-mask -->
                    </div><!-- /col -->


                    <!-- MODAL SHOW THE PORTFOLIO IMAGE. In this demo, all links point to this modal. You should create
                         a modal for each of your projects. -->

                    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Sciflor - I Simpósio de Ciências Florestais do Estado do Espírito Santo</h4>
                                </div>
                                <div class="modal-body">
                                    <p><img class="img-responsive" src="assets/img/portfolio/sciflor.png" alt=""></p>
                                    <p>Website desenvolvido para o I Simpósio de Ciências Florestais do Estado do Espírito Santo, desenvolvido em linguagem web que se auto ajusta à tela de visualização, assim, otimizado para smartphones e tablets. </p>
                                    <p><a href="http://www.sciflor.com.br">Visitar o site</a></p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->


                    <!-- PORTFOLIO IMAGE 3 -->
                    <div class="col-md-4">
                        <div class="grid mask">

                        </div><!-- /grid-mask -->
                    </div><!-- /col -->
                </div><!-- /row -->

                <!-- PORTFOLIO IMAGE 4 -->
                <div class="row">	
                    <div class="col-md-4 ">
                        <div class="grid mask">

                        </div><!-- /grid-mask -->
                    </div><!-- /col -->
                </div><!-- /row -->
                <br>
                <br>
            </div><!-- /row -->
        </div><!-- /container -->

        <!-- ==== SECTION DIVIDER4 ==== -->

        <div class="container" id="contact" >
            <div class="row">
                <br>
                <h1 class="centered">CONTATO</h1>
                <hr>
                <br>
                <br>
                <div class="col-lg-4">
                    <h3>Informações</h3>
                    <p>
                        <span class="icon icon-mobile"></span> +55 28 99987 7542 <br/>

                        <span class="icon icon-envelop"></span> <a > contato@marcosfabrejr.com.br</a> <br/>

                        <span class="icon icon-facebook"></span> <a href="http://www.facebook.com/timarcosfabrejr"> Marcos Fabre Jr - Soluções em TI </a> <br/>


                    </p>
                    <br />

                    <div class="fb-like " data-href="https://facebook.com/timarcosfabrejr" data-width="300"  data-action="like" data-show-faces="true" data-share="true"></div>

                </div><!-- col -->

                <div class="col-lg-4">
                    <h3>Newsletter</h3>
                    <p>Deseja receber nossas novidades, notícias sobre o mundo tecnológico e as novas formas de 
                        administrar sua empresa?<br /> Cadastre seu Email e receba nosso newsletter.</p>
                    <p>
                    <form class="form-horizontal" role="form" id="form2" action="email.php" method="post">
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-4 control-label"></label>
                            <div class="col-lg-10">
                                <input type="email" class="form-control" id="email2" name="email2"  placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="text1" class="col-lg-4 control-label"></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="nome2" name="nome2" placeholder="Seu Nome">
                                <input type="text"  id="confirm" name="confirm" hidden value="NEWSLETTER!!!!">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10">
                                <button type="submit" class="btn btn-success">Enviar</button>
                            </div>
                        </div>
                    </form><!-- form -->

                </div><!-- col -->

                <div class="col-lg-4">
                    <h3>Contate-nos</h3>
                    <p>Deseja ter mais facilidade na administração do seu negócio? Faça contato conosco e descubra soluções 
                        para suas necessidades. </p>
                    <form class="form-horizontal" role="form" id="form1"  action="email.php" method="post">
                        <div class="form-group">
                            <label for="inputEmail1" class="col-lg-3 control-label"></label>
                            <div class="col-lg-10">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="text1" class="col-lg-4 control-label"></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="nome" name="nome" placeholder="Seu Nome">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="text1" class="col-lg-4 control-label"></label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="telefone" name="telefone" placeholder="Telefone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="text1" class="col-lg-4 control-label"></label>
                            <div class="col-lg-10">
                                <textarea placeholder="mensagem" id="mensagem" name="mensagem" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-10">
                                <button type="submit" class="btn btn-success">Enviar</button>
                            </div>
                        </div>
                    </form><!-- form -->
                </div><!-- col -->

            </div><!-- row -->

        </div><!-- container -->

        <?php require_once 'layout/rodape.php'; ?>

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->


        <script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/retina.js"></script>

        <script type="text/javascript" src="assets/js/smoothscroll.js"></script>
        <script type="text/javascript" src="assets/js/jquery-func.js"></script>
    </body>
</html>
