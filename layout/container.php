<section style="margin-top: 25px;margin-bottom: 25px;">    
    <div class="container center-align">
        
            <div class="col-md-3 col-sm-6 center-align center-child services-index">
                <img class="img-responsive center-child" src="img/icon/grafico.png">        
                <div class="icon-title">
                    Search Marketing
                </div>
                <div class="icon-text">
                    A Alvo otimiza o código a fim de aumentar o tráfego de clientes potenciais no seu website, utilizando Search Engine Optimization (SEO).
                </div>
            </div>
            <div class="col-md-3 col-sm-6 center-align services-index">
                <img class="img-responsive center-child" src="img/icon/shopping.png">
                <div class="icon-title">
                    Lojas Virtuais
                </div>
                <div class="icon-text">
                 Venda todos os dias, 24 horas por dia. Com uma loja virtual as chances de crescimento do seu negócio multiplicam. Portabilidade e confiança para seu cliente.
                </div>
            </div>
            <div class="col-md-3 col-sm-6 center-align services-index">
                <img class="img-responsive center-child" src="img/icon/website.png">
                <div class="icon-title">
                    Criação de Sites
                </div>
                <div class="icon-text">
                Possuir uma identidade virtual é de suma importância para qualquer empresa ou profissional liberal que almeja o sucesso. 
                </div>
            </div>
            <div class="col-md-3 col-sm-6 center-align services-index">
                <img class="img-responsive center-child" src="img/icon/programming.png">
                <div class="icon-title">
                    Sistemas Web
                </div>
                <div class="icon-text">
               Sistemas gerenciais desenvolvidos especificamente para sua empresa. Liberdade para gerenciar seu negócio de qualquer lugar você só encontra na Alvo.
                </div>
            </div>
      
    </div>

</section>

<section class="services-social" style="height: 200px;margin-bottom: 100px;">
    
</section>
