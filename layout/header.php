<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<base href="http://alvodesenvolvimento.com.br">
<meta name="author" content="Alvo Desenvolvimento">   
<link rel="stylesheet" href="css/style.css" type="text/css">       
<script src="js/jquery.js"></script>
<link rel="stylesheet" href="plugins/slick/slick.css" type="text/css">
<script src="plugins/slick/slick.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/creative.js"></script>