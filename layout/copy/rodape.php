


<div id="footer-map" class="full" >
     <div class="separator-50"></div>
    <div class="container map">
       
        <div class="col-lg-3 col-md-3 col-xs-12"style="text-align: left; ">

            
            <p class="font-responsive theme-font" style=""> (28)&nbsp;&nbsp; 3511-2293 <br>(28) 99976-0135 <br>(28) 99955-2533</p>
            
        </div>
        <div class="col-lg-3 col-md-3 col-xs-12 border-right border-left theme-font height-map " style="color: #ccc;">
            <h4>NK Informática</h4>
            <ul style="">

                <li><a href="#">Quem Somos</a>
                    <ul style="">
                          <li><a href="#">Institucional</a></li>
                <li><a href="#">Equipe</a></li>
                    </ul>
                </li>
                
                
              
                <li><a href="#">Serviços</a></li>
                <li><a href="#">Parceiros</a></li>
                <li><a href="#">Clientes</a></li>
                <li><a href="#">Notícias</a></li>
                <li><a href="#">Fale Conosco</a></li>
                
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-xs-12 border-right height-map theme-font" style="color: #ccc; ">
            <h4>Contato:</h4>
            <ul>

                <li style="list-style: none;">NK Informática</li>
                <li style="list-style: none;">Av. Carlos Lindenberg 50, Novo Parque<br> Cachoeiro de Itapemirim–ES<br>Cep: 29.309-110</li>
                

            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-xs-12 height-map-end " style="color: #ccc;">
            <h4>Redes Sociais:</h4>
            <ul>
              


      <div class="btn btn-block btn-social btn-facebook">
        <i class="fa fa-facebook"></i> Facebook
      </div>
       <div class="btn btn-block btn-social btn-google-plus">
        <i class="fa fa-youtube"></i> Youtube
      </div>
     <div class="btn btn-block btn-social btn-instagram">
        <i class="fa fa-instagram"></i> Instagram
      </div>
       <div class="btn btn-block btn-social btn-twitter">
        <i class="fa fa-twitter"></i> Twitter
      </div>
      

            </ul>
        </div>
    </div>

    <div class="separator-10"></div>

    <div class="full border-top"></div>
    <div class="container" >
        <div class="separator-10"></div>
        <div class="col-lg-5 theme-font footer-style" style="text-align: left;color: white"><div class="separator-10" style="text-align: left; padding-top: 15px"></div>&copy; 2015<b> - NK Informática - </b>Todos os direitos reservados.  </div>  

        <div class="col-lg-1 col-md-1 pull-right theme-font footer-style "> <a target="_blank" href="http://www.arcoinformatica.com.br"><div class="arco"></div></a></div>
        <div style="height: 50px"></div>

    </div>

</div>

   

