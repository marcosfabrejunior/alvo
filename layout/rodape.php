<footer>
    <div class="full" style="background-color: #02111D">
        <div class="container map">
            <div class="separator-50"></div>
            <div class="col-lg-3 col-md-3 "style="margin-top: ">
                <a href="index.php">     <img class="img-responsive" src="img/alvoaa.png" style="width: 200px"></a>
                <div class="separator-50"></div>

                <p class="theme-font" style="font-size: 25px;color: white; font-weight: none !important;">(28) 99987-7542</p>
                <p class="theme-font" style="font-size: 25px;color: white; font-weight: none !important;margin-top: -20px;">(28) 99941-7185</p>
            </div>
            <div class="col-lg-3 col-md-3 border-right border-left theme-font height-map " style="color: #ccc;">
                <h4>Alvo Desenvolvimento</h4>
                <ul >
                    <a href="#">  <li>Alvo</li></a>
                    <a href="#">  <li>Pessoas</li></a>
                    <a href="#">  <li>Organização</li></a>
                    <a href="#">  <li>Websites</li></a>
                    <a href="#">  <li>Sistemas</li></a>
                    <a href="#">  <li>Serviços</li></a>
                    <a href="#">  <li>Utilidade Pública</li></a>
                    <a href="#">  <li>Atendimento</li></a>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 border-right height-map" style="color: #ccc;">
                <h4>Contato:</h4>
                <ul>
                    <li style="list-style: none;">contato@alvodesenvolvimento.com.br</li>
                    <li style="list-style: none;">(28) 99987-7542</li>
                    <li style="list-style: none;">(28) 99941-7185</li>
                    <li style="list-style: none;">Rua Hilarina Martins Bueno, Nº54, Amaral <br> Cachoeiro de Itapemirim–ES<br>Cep: 29.305-150</li>
                </ul>
            </div>
            <div class="col-lg-2 col-md-2 height-map" style="color: #ccc;">
                <h4>Redes Sociais:</h4>
                <ul>
                    <a href="#"><li style="list-style: none;"><img style="width: 30px;" src="img/icon/facebook.png"></li></a>
                </ul>
            </div>
        </div>
        <div class="separator-10"></div>
        <div class="full border-top"></div>
        <div class="container" >
            <div class="separator-10"></div>
            <div class="col-lg-5 theme-font footer-style" style="text-align: left"><div class="separator-10" style="text-align: left"></div>&copy; 2015<b> - Alvo Desenvolvimento - </b>Todos os direitos reservados.  </div>
            <div class="col-lg-1 col-md-1 pull-right theme-font footer-style "><a href="#"><img style="max-width: 100px" src="img/developer-hover.png"></a></div>
            <div style="height: 50px"></div>
        </div>
    </div>
</footer>
<?php ob_flush(); ?>