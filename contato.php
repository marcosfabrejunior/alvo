<html>
    <head>
        <?php include 'layout/header.php'; ?>
        <title>Alvo Desenvolvimento - Contato</title>
    </head>
    <body style="overflow-x: hidden;">
        <?php
        require_once 'layout/topo.php';
        ?>
        <div class="separator-50"></div>

        <div style="height: 50px;"></div>
        <div class="row">
            <div class="container">
                <div class="col-lg-3 pull-left">
                    <h2 style="color: #243A7F;">Fale Conosco</h2>   
                </div>     
            </div>
            <div class="container horizontal-padding">
                <div  style="height: 1px; width: 100%; background-color: black;"></div>

            </div>
        </div>
        <div class="separator-50"></div>
        <div class="container">

            <div class="col-lg-6" style="text-align: center">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d932.0780504371288!2d-41.10797782933687!3d-20.859462859279475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xb942c5cffa040d%3A0x3dd03438a67d5b47!2sAlvo+Desenvolvimento!5e0!3m2!1spt-BR!2sbr!4v1444775056310" width="100%" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="col-lg-6">
                <div class="full" style="text-align: center">
                    <div style="display: inline-block">
                        <p class="theme-font" style="color:#243A7F;font-size: 18px;">Preencha o formulário abaixo para nos enviar sua mensagem</p>
                    </div>
                </div>
                <form class="form-group" id="form-contato" action="mailSend.php" method="post">
                    <div class="col-lg-12 padding-form">
                        <input name="nome" id="nome" class="form-control" type="text" autofocus required placeholder="Nome">
                    </div>
                    <div  class="col-lg-12 padding-form">
                        <input name="telefone" id="telefone" class="form-control" type="text" autofocus required placeholder="Telefone">
                    </div>
                    <div class="col-lg-12 padding-form">
                        <input name="email" id="email" class="form-control" type="email" autofocus required placeholder="E-mail">
                    </div>
                    <div class="col-lg-12 padding-form">
                        <textarea name="mensagem" id="mensagem"  class="form-control" required placeholder="Mensagem" rows="5"></textarea>
                    </div>
                </form>

            </div>
        </div>

        <div style="height: 50px;"></div>

        <?php include 'layout/rodape.php'; ?>

        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="js/jquery.easing.min.js"></script>


        <!-- Custom Theme JavaScript -->
        <script src="js/creative.js"></script>

    </body>

</html>