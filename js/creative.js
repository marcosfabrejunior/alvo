
    
   jQuery(document).ready(function($){
       
          var nav = $('#topo');
          var nav2 = $('.menu-navbar');
          var nav3 = $('.img_topo');
 
    $(window).scroll(function () {
        if ($(this).scrollTop() > 2) {
            nav.addClass("scrolled");
            nav2.addClass("menu-navbar-scrolled");
            nav3.addClass("img_topo_scrolled");
         
        } else {
            nav.removeClass("scrolled");
            nav2.removeClass("menu-navbar-scrolled");
            nav3.removeClass("img_topo_scrolled");
        }
    });
       
       if($('div#painel .painel-case').length){
        $('div#painel .painel-case').slick({
            dots: true,
            autoplay: true,
            autoplaySpeed: 6000,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: false,
            swipeToSlide: true,
            easing: 'easeOutCirc',
            prevArrow: '<a class="pager left t02"></a>',
            nextArrow: '<a class="pager right t02"></a>',
        });
    }
    
    if($('div#painel-products .painel-case').length){
        $('div#painel-products .painel-case').slick({
            dots: false,
            autoplay: true,
            autoplaySpeed: 1000,
            speed: 2000,
            slidesToShow: 1,
            slidesToScroll: 1,
            pauseOnHover: true,
            swipeToSlide: true,
            centerMode:true,
            centerPadding:'10px',
            easing: 'easeOutCirc',
            prevArrow: '<a class="pager left t02"></a>',
            nextArrow: '<a class="pager right t02"></a>',
            
        });
    }
    
       
});
 