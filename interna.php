<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="Alvo Desenvolvimento">
        <title> Alvo Desenvolvimento</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body style="overflow-x: hidden;">
        
        <?php
        require_once 'layout/topo.php';

        ?>
        <div style="height: 80px;"></div>
        
        
        <div class="row">
            <div class="container">
            <div class="col-lg-3 pull-left">
            <h2>Página Interna</h2>   
            </div>     
            </div>
            <div class="container horizontal-padding">
                <div  style="height: 1px; width: 100%; background-color: black;"></div>
        
            </div>
        </div>
        
        <div style="height: 100%;"></div>
        
        
        <?php include 'layout/rodape.php'; ?>

        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="js/jquery.easing.min.js"></script>


        <!-- Custom Theme JavaScript -->
        <script src="js/creative.js"></script>

    </body>

</html>